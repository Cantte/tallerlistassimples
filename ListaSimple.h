#ifndef LISTA_H
#define LISTA_H

#include "Nodo.h"
#include "Iterador.h"

template<class T> class ListaSimple
{
public:

	using IteradorLista = Iterador<T>;

	ListaSimple()
	{
		m_cab = nullptr;
		m_ult = nullptr;
	}

	~ListaSimple()
	{
		Nodo<T>* q = GetCab();
		while (q != nullptr)
		{
			Nodo<T>* p = q->GetSgte();
			EliminarNodo(q);
			q = p;
		}
	}

	Nodo<T>* GetCab() const
	{
		return m_cab;
	}

	IteradorLista GetInicio() const
	{
		return Iterador<T>(GetCab());
	}

	IteradorLista GetFinal() const
	{
		return Iterador<T>(GetUlt()->GetSgte());
	}

	Nodo<T>* GetUlt() const
	{
		return m_ult;
	}

	void SetCab(Nodo<T>* cab)
	{
		m_cab = cab;
	}

	void SetUlt(Nodo<T>* ult)
	{
		m_ult = ult;
	}

	void Insertar(T dato)
	{
		Nodo<T>* p = new Nodo<T>(dato);
		if (EstaVacia())
		{
			SetCab(p);
			SetUlt(p);
		}
		else
		{
			GetUlt()->SetSgte(p);
			SetUlt(p);
		}
	}

	void InsertarFrente(T dato)
	{
		Nodo<T>* p = new Nodo<T>(dato);
		p->SetSgte(GetCab());
		SetCab(p);
	}

	void InsertarFin(T dato)
	{
		Nodo<T>* p = new Nodo<T>(dato);
		GetUlt()->SetSgte(p);
		SetUlt(p);
	}

	void InsertarOrdA(T dato)
	{
		if (EstaVacia())
			Insertar(dato);
		else
		{
			if (dato <= GetCab()->GetDato())
			{
				InsertarFrente(dato);
			}
			else if (dato >= GetUlt()->GetDato())
			{
				InsertarFin(dato);
			}
			else
			{
				Nodo<T>* p = new Nodo<T>(dato);
				Nodo<T>* q = GetCab();
				while (p->GetDato() >= q->GetDato())
					q = q->GetSgte();
				Nodo<T>* ant = Anterior(q);
				ant->SetSgte(p);
				p->SetSgte(q);
			}
		}
	}

	void InsertaOrdD(T dato)
	{
		if (EstaVacia())
			Insertar(dato);
		else
		{
			if (dato >= GetCab()->GetDato())
			{
				InsertarFrente(dato);
			}
			else if (dato <= GetUlt()->GetDato())
			{
				InsertarFin(dato);
			}
			else
			{
				Nodo<T>* p = new Nodo<T>(dato);
				Nodo<T>* q = GetCab();
				while (p->GetDato() <= q->GetDato())
					q = q->GetSgte();
				Nodo<T>* ant = Anterior(q);
				ant->SetSgte(p);
				p->SetSgte(q);
			}
		}
	}

	void Ordenar()
	{

	}

	bool Contiene(T dato)
	{
		return Buscar(dato) != nullptr;
	}

	Nodo<T>* Buscar(T dato)
	{
		if (EstaVacia())
			return nullptr;
		Nodo<T>* p = GetCab();
		while (p != nullptr)
		{
			if (p->GetDato() == dato)
				return p;
			p = p->GetSgte();
		}
		return p;
	}

	bool Eliminar(T dato)
	{
		Nodo<T>* q = Buscar(dato);
		if (q == nullptr)
			return false;
		EliminarNodo(q);
		return true;
	}

	void EliminarNodo(Nodo<T>* nodo)
	{
		if (EstaVacia())
			return;
		if (nodo == GetCab())
		{
			SetCab(GetCab()->GetSgte());
			delete nodo;
		}
		else
		{
			Nodo<T>* r = Anterior(nodo);
			r->SetSgte(nodo->GetSgte());
			if (r->GetSgte() == nullptr) /*Es el nuevo ultimo?*/
				SetUlt(r);
			delete nodo;
		}
	}

	void EliminarTodos(T dato)
	{
		Nodo<T>* q = GetCab();
		while (q != nullptr)
		{
			if (q->GetDato() == dato)
			{
				Nodo<T>* p = q->GetSgte();
				EliminarNodo(q);
				q = p;
			}
			else
				q = q->GetSgte();
		}
	}

	void EliminarRepetidos()
	{
		int count = 0;
		Nodo<T>* q = GetCab();
		while (q != nullptr)
		{
			count = Cuantos(q->GetDato());
			if (count > 1)
			{
				Nodo<T>* p = q->GetSgte();
				EliminarTodos(q->GetDato());
				q = p;
			}
			else
				q = q->GetSgte();
		}
	}

	int Cuantos(T dato)
	{
		if (EstaVacia())
			return -1;
		int count = 0;
		Nodo<T>* q = GetCab();
		while (q != nullptr)
		{
			if (q->GetDato() == dato)
				count++;
			q = q->GetSgte();
		}
		return count;
	}

	void Actualizar(T datoN)
	{
		if (EstaVacia())
			return;
		Nodo<T>* p = Buscar(datoN);
		p->SetDato(datoN);
	}

	Nodo<T>* Anterior(Nodo<T>* nodo)
	{
		if (EstaVacia() || nodo == GetCab())
			return nullptr;
		else
		{
			Nodo<T>* q = GetCab();
			while (q->GetSgte() != nodo)
				q = q->GetSgte();
			return q;
		}
	}

	bool EstaVacia()
	{
		return m_cab == nullptr;
	}
private:
	Nodo<T>* m_cab;
	Nodo<T>* m_ult;
	int size;
};

#endif // !LISTA_H
