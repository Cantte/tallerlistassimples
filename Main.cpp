#include <conio.h>
#include <iostream>
#include <string>
#include <Windows.h>


#include "ListaSimple.h"

using namespace std;

/*Estructuras de datos*/
struct Medicamento
{
	int referencia;
	char nombre[50];
	char presentacion[50];
	int cantidadExistente;
	int cantidadMaxima;
	int cantidadMinima;

	bool operator==(const Medicamento& other) { return referencia == other.referencia; }
};

struct ProductoEnBodega
{
	int codigo;
	char nombre[50];
	int cantidad;

	bool operator==(const ProductoEnBodega& other) { return codigo == other.codigo; }
};

struct ProductoVendido
{
	int codigo;
	int ventas;

	bool operator==(const ProductoVendido& other) { return codigo == other.codigo; }
};

struct DatoAccion
{
	int dato;
	char accion;
};

struct Estudiante
{
	int codigo;
	char nombre[50];
	int grupo;
	int semestre;

	bool operator==(const Estudiante& other) { return codigo == other.codigo; }
};

/*Listas a usar*/
/*Ejercicio 1*/
ListaSimple<Medicamento> inventarioDeMedicamento;
/*Ejercicio 2 y 3*/
ListaSimple<int> listaEnteros, listaEnteros2;
/*Ejercicio 4*/
ListaSimple<string> listaNombres;
/*Ejercicio 5*/
ListaSimple<ProductoEnBodega> productosEnBodega;
ListaSimple<ProductoVendido> productosVendidos;
/*Ejercicio 6*/
ListaSimple<int> cab01, cab02;
/*Ejercicio 7*/
ListaSimple<int> cab1;
ListaSimple<DatoAccion> cab2;
/*Ejercicio 8*/
ListaSimple<Estudiante> listaEstudiantes;
ListaSimple<string> estudiantesBorrados;

/*Estrutura utilitaria*/
struct Vector2D
{
	Vector2D(short _x, short _y) : x(_x), y(_y) { }
	short x, y;
};

/*Funciones utilitarias*/
auto GoToXY = [](Vector2D pos) { SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), { pos.x, pos.y }); };

void PaintInterface();
void PrintfCenterText(const string& _text, short posY);
short GetPosWhereCenter(const string& _text);
void PrintfErrOrSucc(const string& err);

/*Funciones principales*/
void MainMenu();

void Ejercicio1();
void SubMenu1();
void RegistrarMedicamento();

void Ejercicio2();
void Ejercicio3();
void Ejercicio4();
void Ejercicio5();
void Ejercicio6();
void Ejercicio7();
void Ejercicio8();
void RegistrarEstudiante();

/*Otras funcions utilitarias*/
template<class T> void MostrarLista(const ListaSimple<T>& lista, string titulo);
void MostrarLista(const ListaSimple<Medicamento>& lista, string titulo);
void MostrarLista(const ListaSimple<ProductoEnBodega>& lista, string titulo);
void MostrarLista(const ListaSimple<ProductoVendido>& lista, string titulo);
void MostrarLista(const ListaSimple<DatoAccion>& lista, string titulo);
void MostrarLista(const ListaSimple<Estudiante>& lista, string titulo);

int main()
{
	SetConsoleTitle("Taller listas enlazadas simples");
	MainMenu();
	_getch();
	return EXIT_SUCCESS;
}

void PaintInterface()
{
	system("cls");
	short positionPaint = 0;

	for (positionPaint = 2; positionPaint < 78; positionPaint++)
	{
		GoToXY({ positionPaint,4 });	cout << char(205);
		GoToXY({ positionPaint, 34 });	cout << char(205);
	}

	for (positionPaint = 4; positionPaint < 34; positionPaint++)
	{
		GoToXY({ 2, positionPaint });	cout << char(186);
		GoToXY({ 77,positionPaint });	cout << char(186);
	}

	GoToXY({ 2, 4 });		cout << char(201);
	GoToXY({ 2, 34 });		cout << char(200);
	GoToXY({ 77, 4 });		cout << char(187);
	GoToXY({ 77, 34 });		cout << char(188);
}

void PrintfCenterText(const string& _text, short posY)
{
	GoToXY({ GetPosWhereCenter(_text), posY });	cout << _text;
}

short GetPosWhereCenter(const string& _text)
{
	short nSpaces = 0;

	nSpaces = static_cast<short>(_text.size() + 1);
	nSpaces = (77 - nSpaces) / 2;

	return ((nSpaces % 2 == 0) ? nSpaces++ : nSpaces);
}

void PrintfErrOrSucc(const string& err)
{
	PrintfCenterText(err, 33);
	Sleep(1000);
}

void MainMenu()
{
	int op = 0;
	do
	{
		PaintInterface();
		PrintfCenterText("***MENU PRINCIPAL***", 2);
		PrintfCenterText("Bienvenido, digite la opcion que desee", 6);
		for (short i = 1; i <= 8; i++)
		{
			GoToXY({ 10, 8 + i });
			cout << i << ".Resolver ejercicio " << i << endl;
		}
		GoToXY({ 10, 17 });
		cout << "9.Salir\n";
		GoToXY({ 10, 19 });
		cout << "Digite una opcion ? ";
		cin >> op;
		switch (op)
		{
			case 1:
			{
				Ejercicio1();
				break;
			}
			case 2:
			{
				Ejercicio2();
				break;
			}
			case 3:
			{
				Ejercicio3();
				break;
			}
			case 4:
			{
				Ejercicio4();
				break;
			}
			case 5:
			{
				Ejercicio5();
				break;
			}
			case 6:
			{
				Ejercicio6();
				break;
			}
			case 7:
			{
				Ejercicio7();
				break;
			}
			case 8:
			{
				Ejercicio8();
				break;
			}
			case 9:
			{
				PrintfCenterText("SALIENDO DE LA APLICACION....", 25);
				Sleep(500);
				exit(1);
				break;
			}
			default:
				PrintfErrOrSucc("Opcion no valida....");
				break;
		}
	} while (op != 9);
}

void Ejercicio1()
{
	const char* opciones[] = 
	{
		"1.Registrar informacion de inventario",
		"2.Gestion de inventario",
		"3.Regresar..."
	};

	int op = 0;
	do
	{
		PaintInterface();
		PrintfCenterText("***MENU PRINCIPAL - GESTION DE MEDICAMENTOS***", 2);
		PrintfCenterText("Digite la opcion que desee", 6);
		for (short i = 0; i < 3; i++)
		{
			GoToXY({ 15, 10 + i });
			cout << opciones[i];
		}
		GoToXY({ 15, 15 });
		cout << "Digite una opcion: ";
		cin >> op;

		switch (op)
		{
		case 1:
		{
			RegistrarMedicamento();
			break;
		}
		case 2:
		{
			SubMenu1();
			break;
		}
		case 3:
		{
			break;
		}
		default:
			PrintfErrOrSucc("Opcion no valida...");
			break;
		}

	} while (op != 3);
}

void SubMenu1()
{
	const char* opciones[] =
	{
		"1.Actualizar informacion de un medicamento",
		"2.Borrar un medicamento del inventario",
		"3.Consultar informacion de un medicamento",
		"4.Informe de inventario",
		"5.Regresar"
	};

	int op = 0;
	do
	{
		PaintInterface();
		PrintfCenterText("***SUBMEMENU - GESTION DE MEDICAMENTOS***", 2);
		PrintfCenterText("Digite la opcion que desee", 6);
		for (short i = 0; i < 5; i++)
		{
			GoToXY({ 15, 10 + i });
			cout << opciones[i];
		}
		GoToXY({ 15, 15 });
		cout << "Digite una opcion: ";
		cin >> op;

		switch (op)
		{
		case 1:
		{
			if (inventarioDeMedicamento.EstaVacia())
			{
				PrintfErrOrSucc("Error: inventario de medicamentos vacio");
			}
			else
			{
				PaintInterface();
				PrintfCenterText("***BUSQUEDA DE MEDICAMENTO***", 2);
				Medicamento medicamento;
				GoToXY({ 6, 10 });
				cout << "Digite la referencia del medicamento: ";
				cin >> medicamento.referencia;
				if (!inventarioDeMedicamento.Contiene(medicamento))
				{
					PrintfErrOrSucc("Error: el medicamento no esta registrado...");

				}
				else
				{
					medicamento = inventarioDeMedicamento.Buscar(medicamento)->GetDato();
					const char* opcionesModificacion[] =
					{
						"1.Modificar nombre",
						"2.Modificar presentacion",
						"3.Modificar cantidad maxima",
						"4.Modificar cantidad minima",
						"5.Modificar cantidad existente",
						"6.Regresar"
					};
					PaintInterface();
					PrintfCenterText("***SUBMEMENU - ACTUALIZACION DE DATOS DE MEDICAMENTO***", 2);
					for (short i = 0; i < 5; i++)
					{
						GoToXY({ 15, 10 + i });
						cout << opcionesModificacion[i];
					}
					int op2;
					GoToXY({ 15, 15 });
					cout << "Digite una opcion: ";
					cin >> op2;

					switch (op2)
					{
					case 1:
					{
						PaintInterface();
						PrintfCenterText("*** ACTUALIZACION DE DATOS DE MEDICAMENTO ***", 2);
						GoToXY({ 6, 10 });
						cout << "Digite el nuevo nombre del medicamento: ";
						cin.ignore();
						cin.getline(medicamento.nombre, 50, '\n');
						inventarioDeMedicamento.Actualizar(medicamento);
						PrintfErrOrSucc("Exito: Nombre del medicamento actualizado....");
						break;
					}
					case 2:
					{
						PaintInterface();
						PrintfCenterText("*** ACTUALIZACION DE DATOS DE MEDICAMENTO ***", 2);
						GoToXY({ 6, 10 });
						cout << "Digite la nueva presentacion del medicamento: ";
						cin.ignore();
						cin.getline(medicamento.presentacion, 50, '\n');
						inventarioDeMedicamento.Actualizar(medicamento);
						PrintfErrOrSucc("Exito: Presentacion del medicamento actualizada....");
						break;
					}
					case 3:
					{
						PaintInterface();
						PrintfCenterText("*** ACTUALIZACION DE DATOS DE MEDICAMENTO ***", 2);
						GoToXY({ 6, 10 });
						cout << "Digite la nueva cantidad maxima de medicamento: ";
						cin >> medicamento.cantidadMaxima;
						inventarioDeMedicamento.Actualizar(medicamento);
						PrintfErrOrSucc("Exito: Cantidad maxima de medicamento actualizada....");
						break;
					}
					case 4:
					{
						PaintInterface();
						PrintfCenterText("*** ACTUALIZACION DE DATOS DE MEDICAMENTO ***", 2);
						GoToXY({ 6, 10 });
						cout << "Digite la nueva cantidad minima de medicamento: ";
						cin >> medicamento.cantidadMinima;
						inventarioDeMedicamento.Actualizar(medicamento);
						PrintfErrOrSucc("Exito: Cantidad minima de medicamento actualizada....");
						break;
					}
					case 5:
					{
						PaintInterface();
						PrintfCenterText("*** ACTUALIZACION DE DATOS DE MEDICAMENTO ***", 2);
						GoToXY({ 6, 10 });
						cout << "Digite la nueva cantidad existente de medicamento: ";
						cin >> medicamento.cantidadExistente;
						inventarioDeMedicamento.Actualizar(medicamento);
						PrintfErrOrSucc("Exito: Cantidad existente de medicamento actualizada....");
						break;
					}
					case 6:
					{
						break;
					}
					default:
						PrintfErrOrSucc("Opcion no valida...");
						break;
					}

				}
			}
			break;
		}
		case 2:
		{
			if (inventarioDeMedicamento.EstaVacia())
			{
				PrintfErrOrSucc("Error: inventario de medicamentos vacio");
			}
			else
			{
				PaintInterface();
				PrintfCenterText("***BUSQUEDA DE MEDICAMENTO - ELIMINACION***", 2);
				Medicamento medicamento;
				GoToXY({ 6, 10 });
				cout << "Digite la referencia del medicamento: ";
				cin >> medicamento.referencia;
				if (!inventarioDeMedicamento.Contiene(medicamento))
				{
					PrintfErrOrSucc("Error: el medicamento no esta registrado...");

				}
				else
				{
					GoToXY({ 6, 13 });
					cout << "Esta seguro de borrar el medicamento [S/N] ? ";
					char resp = _getch();
					if (toupper(resp) == 'S')
					{
						if (inventarioDeMedicamento.Eliminar(medicamento))
						{
							PrintfErrOrSucc("Exito! Medicamento eliminado del inventario...");
						}
						else
						{
							PrintfErrOrSucc("Error: El medicamento no ha sido eliminado...");
						}
					}
				}

				break;
			}
		case 3:
		{
			if (inventarioDeMedicamento.EstaVacia())
			{
				PrintfErrOrSucc("Error: inventario de medicamentos vacio");
			}
			else
			{
				PaintInterface();
				PrintfCenterText("***BUSQUEDA DE MEDICAMENTO - VISUALIZACION***", 2);
				Medicamento medicamento;
				GoToXY({ 6, 10 });
				cout << "Digite la referencia del medicamento: ";
				cin >> medicamento.referencia;
				if (!inventarioDeMedicamento.Contiene(medicamento))
				{
					PrintfErrOrSucc("Error: el medicamento no esta registrado...");

				}
				else
				{
					medicamento = inventarioDeMedicamento.Buscar(medicamento)->GetDato();
					PaintInterface();
					PrintfCenterText("*** VISUALIZACION DE MEDICAMENTO ***", 2);
					GoToXY({ 7, 10 });
					cout << "Referencia: " << medicamento.referencia;
					GoToXY({ 7, 12 });
					cout << "Nombre: " << medicamento.nombre;
					GoToXY({ 7, 14 });
					cout << "Presentacion: " << medicamento.presentacion;
					GoToXY({ 7, 16 });
					cout << "Cantidad minima: " << medicamento.cantidadMinima;
					GoToXY({ 7, 18 });
					cout << "Cantidad maxima: " << medicamento.cantidadMaxima;
					GoToXY({ 7, 20 });
					cout << "Cantidad existente: " << medicamento.cantidadExistente;
					PrintfCenterText("Presione una tecla para continuar...", 25);
					_getch();
				}
			}

			break;
		}
		case 4:
		{
			if (inventarioDeMedicamento.EstaVacia())
			{
				PrintfErrOrSucc("Error: inventario de medicamentos vacio...");
			}
			else
			{
				MostrarLista(inventarioDeMedicamento, "***MEDICAMENTOS REGISTRADOS****");
			}
			break;
		}
		case 5:
		{
			break;
		}
		default:
			PrintfErrOrSucc("Opcion no valida...");
			break;
		}

		}
	} while (op != 5);
}

void RegistrarMedicamento()
{
	PaintInterface();
	PrintfCenterText("***REGISTRO DE MEDICAMENTO***", 2);
	Medicamento medicamento;
	GoToXY({ 6, 10 });
	cout << "Digite la referencia del medicamento: ";
	cin >> medicamento.referencia;
	while (inventarioDeMedicamento.Contiene(medicamento))
	{
		PrintfErrOrSucc("Error: Medicamento ya registrado...");
		PaintInterface();
		PrintfCenterText("***REGISTRO DE MEDICAMENTO***", 2);
		GoToXY({ 6, 10 });
		cout << "Digite la referencia del medicamento: ";
		cin >> medicamento.referencia;
	}
	GoToXY({ 6, 11 });
	cout << "Digite el nombre del medicamento: ";
	cin.ignore();
	cin.getline(medicamento.nombre, 50, '\n');
	GoToXY({ 6,12 });
	cout << "Digite la presentacion del medicamento: ";
	cin.ignore();
	cin.getline(medicamento.presentacion, 50, '\n');
	GoToXY({ 6, 13 });
	cout << "Digite la cantidad minima del medicamento: ";
	cin >> medicamento.cantidadMinima;
	GoToXY({ 6, 14 });
	cout << "Digite la cantidad maxima del medicamento: ";
	cin >> medicamento.cantidadMaxima;
	GoToXY({ 6, 15 });
	cout << "Digite la cantidad existente del medicamento: ";
	cin >> medicamento.cantidadExistente;


	inventarioDeMedicamento.Insertar(medicamento);
}

void Ejercicio2()
{
	const char* opciones[] = {
		"1.Ingresar un numero",
		"2.Borrar un numero",
		"3.Mostar numeros",
		"4.Salir"
	};

	int op = 0;

	do
	{
		PaintInterface();
		PrintfCenterText("****EJERCICIO 2 - SOLUCION****", 2);
		for (short i = 0; i < 4; i++)
		{
			GoToXY({ 15, 10 + i });
			cout << opciones[i];
		}
		GoToXY({ 15, 15 });
		cout << "Digite una opcion: ";
		cin >> op;

		switch (op)
		{
			case 1:
			{
				PaintInterface();
				PrintfCenterText("****EJERCICIO 2 - SOLUCION****", 2);
				GoToXY({ 6, 10 });
				cout << "Digite el numero a agregar: ";
				int num = 0;
				cin >> num;
				listaEnteros.Insertar(num);
				break;
			}
			case 2:
			{
				if (listaEnteros.EstaVacia())
					PrintfErrOrSucc("Error: Lista de enteros vacia...");
				else
				{
					PaintInterface();
					PrintfCenterText("****EJERCICIO 2 - SOLUCION****", 2);
					GoToXY({ 6, 10 });
					cout << "Digite el numero a eliminar: ";
					int num = 0;
					cin >> num;

					if (listaEnteros.Eliminar(num))
						PrintfErrOrSucc("El elemento ha sido eliminado de la lista...");
					else
						PrintfErrOrSucc("Error: La lista no contiene el elemento...");
				}
				break;
			}
			case 3:
			{
				if (listaEnteros.EstaVacia())
					PrintfErrOrSucc("Error: Lista de enteros vacia...");
				else
				{
					PaintInterface();
					PrintfCenterText("****EJERCICIO 2 - SOLUCION****", 2);
					MostrarLista(listaEnteros, "****NUMEROS REGISTRADOS****");
				}
				break;
			}
			case 4:
			{
				break;
			}
			default:
				PrintfErrOrSucc("Opcion no valida");
				break;
		}

	} while (op != 4);
}

void Ejercicio3()
{
	const char* opciones[] = {
		"1.Ingresar un numero",
		"2.Borrar todos los repetidos",
		"3.Mostar numeros",
		"4.Salir"
	};

	int op = 0;

	do
	{
		PaintInterface();
		PrintfCenterText("****EJERCICIO 3 - SOLUCION****", 2);
		for (short i = 0; i < 4; i++)
		{
			GoToXY({ 15, 10 + i });
			cout << opciones[i];
		}
		GoToXY({ 15, 15 });
		cout << "Digite una opcion: ";
		cin >> op;

		switch (op)
		{
			case 1:
			{
				PaintInterface();
				PrintfCenterText("****EJERCICIO 3 - SOLUCION****", 2);
				GoToXY({ 6, 10 });
				cout << "Digite el numero a agregar: ";
				int num = 0;
				cin >> num;
				listaEnteros2.Insertar(num);
				break;
			}
			case 2:
			{
				if (listaEnteros2.EstaVacia())
					PrintfErrOrSucc("Error: Lista de enteros vacia...");
				else
				{
					PaintInterface();
					PrintfCenterText("****EJERCICIO 3 - SOLUCION****", 2);
					PrintfCenterText("ELIMINANDO REPETIDOS...", 12);
					listaEnteros2.EliminarRepetidos();
					Sleep(300);
					PrintfErrOrSucc("REPETIDOS ELIMINADOS...");
					Sleep(300);
				}
				break;
			}
			case 3:
			{
				if (listaEnteros2.EstaVacia())
				{
					PrintfErrOrSucc("Error: Lista de enteros vacia...");
				}
				else
				{
					PaintInterface();
					PrintfCenterText("****EJERCICIO 3 - SOLUCION****", 2);
					MostrarLista(listaEnteros2, "****NUMEROS REGISTRADOS****");
				}
				break;
			}
			case 4:
			{
				break;
			}
			default:
				PrintfErrOrSucc("Opcion no valida");
				break;
		}

	} while (op != 4);

}

void Ejercicio4()
{
	const char* opciones[] = {
		"1.Ingresar un nombre",
		"2.Mostar nombres",
		"3.Salir"
	};

	int op = 0;

	do
	{
		PaintInterface();
		PrintfCenterText("****EJERCICIO 4 - SOLUCION****", 2);
		for (short i = 0; i < 3; i++)
		{
			GoToXY({ 15, 10 + i });
			cout << opciones[i];
		}

		GoToXY({ 15, 15 });
		cout << "Digite una opcion: ";
		cin >> op;

		switch (op)
		{
			case 1:
			{
				PaintInterface();
				PrintfCenterText("****EJERCICIO 4 - SOLUCION****", 2);
				PrintfCenterText("***REGISTRO DE NOMBRE DE ESTUDIANTE***", 9);
				GoToXY({ 6, 12 });
				cout << "Digite el nombre del estudiante: ";
				string nombre;
				cin.ignore();
				getline(cin, nombre, '\n');
				if (listaNombres.Contiene(nombre))
				{
					PrintfErrOrSucc("Error: Nombre ya registrado en la lista...");
				}
				else
				{
					listaNombres.Insertar(nombre);
					PrintfErrOrSucc("Nombre insertado a la lista con exito!");
				}
				break;
			}
			case 2:
			{
				if (listaNombres.EstaVacia())
				{
					PrintfErrOrSucc("Error: Lista de nombres vacia...");
				}
				else
				{
					PaintInterface();
					PrintfCenterText("****EJERCICIO 4 - SOLUCION****", 2);
					MostrarLista(listaNombres, "***NOMBRES DE ESTUDIANTES***");
				}
				break;
			}
			case 3:
			{
				break;
			}
			default:
				PrintfErrOrSucc("Opcion no valida...");
				break;
		}

	} while (op != 3);
}

void Ejercicio5()
{
	const char* opciones[] = {
		"1.Registrar un producto en bodega",
		"2.Registrar ventas de un producto",
		"3.Ver productos en bodega",
		"4.Ver venta de productos",
		"5.Actualizar bodega",
		"6.Salir"
	};

	int op = 0;

	do
	{
		PaintInterface();
		PrintfCenterText("****EJERCICIO 5 - SOLUCION****", 2);
		for (short i = 0; i < 6; i++)
		{
			GoToXY({ 15, 10 + i });
			cout << opciones[i];
		}

		GoToXY({ 15, 19 });
		cout << "Digite una opcion: ";
		cin >> op;

		switch (op)
		{
			case 1:
			{
				PaintInterface();
				PrintfCenterText("****EJERCICIO 5 - SOLUCION****", 2);
				PrintfCenterText("****REGISTRO DE PRODUCTO EN BODEGA****", 7);
				ProductoEnBodega producto;
				GoToXY({ 6, 10 });
				cout << "Digite el codigo del producto: ";
				cin >> producto.codigo;
				GoToXY({ 6, 11 });
				cout << "Digite el nombre del produto: ";
				cin.ignore();
				cin.getline(producto.nombre, 50, '\n');
				GoToXY({ 6, 12 });
				cout << "Digite la cantidad en bodega del producto: ";
				cin >> producto.cantidad;
				productosEnBodega.Insertar(producto);
				break;
			}
			case 2:
			{
				PaintInterface();
				PrintfCenterText("****EJERCICIO 5 - SOLUCION****", 2);
				PrintfCenterText("****REGISTRO DE VENTAS DE UN PRODUCTO****", 7);
				ProductoVendido producto;
				GoToXY({ 6, 10 });
				cout << "Digite el codigo del producto: ";
				cin >> producto.codigo;
				GoToXY({ 6, 12 });
				cout << "Digite la cantidad en vendida del producto: ";
				cin >> producto.ventas;
				productosVendidos.Insertar(producto);
				break;
			}
			case 3:
			{
				if (productosEnBodega.EstaVacia())
					PrintfErrOrSucc("Error: No ha registrado productos en bodega...");
				else
					MostrarLista(productosEnBodega, "***PRODUCTOS EN BODEGA REGISTRADOS***");
				break;
			}
			case 4:
			{
				if (productosVendidos.EstaVacia())
					PrintfErrOrSucc("Error: No ha registrado productos vendidos...");
				else
					MostrarLista(productosVendidos, "***PRODUCTOS VENDIDOS***");
				break;
			}
			case 5:
			{
				if (productosEnBodega.EstaVacia() || productosVendidos.EstaVacia())
					PrintfErrOrSucc("Error: No ha registrado productos en bodega o vendidos");
				else
				{
					Nodo<ProductoVendido>* q = productosVendidos.GetCab();
					while (q != nullptr)
					{
						Nodo<ProductoEnBodega>* p = productosEnBodega.GetCab();
						while (p != nullptr)
						{
							if (p->GetDato().codigo == q->GetDato().codigo)
							{
								ProductoEnBodega producto = p->GetDato();
								producto.cantidad -= q->GetDato().ventas;
								if (producto.cantidad < 0)
									producto.cantidad = 0;
								productosEnBodega.Actualizar(producto);
							}
							p = p->GetSgte();
						}
						q = q->GetSgte();
					}
					PrintfErrOrSucc("Productos en bodega actualizados!");
				}
				break;
			}
			case 6:
			{
				break;
			}
			default:
				PrintfErrOrSucc("Opcion no valida");
		}

	} while (op != 6);
}

void Ejercicio6()
{
	const char* opciones[] = {
		"1.Registrar datos en cab1",
		"2.Registrar datos en cab2",
		"3.Ver cab1",
		"4.Ver cab2",
		"5.Cruzar listas",
		"6.Salir"
	};

	int op = 0;

	do
	{
		PaintInterface();
		PrintfCenterText("****EJERCICIO 6 - SOLUCION****", 2);
		for (short i = 0; i < 6; i++)
		{
			GoToXY({ 15, 10 + i });
			cout << opciones[i];
		}

		GoToXY({ 15, 19 });
		cout << "Digite una opcion: ";
		cin >> op;

		switch (op)
		{
			case 1:
			{
				PaintInterface();
				PrintfCenterText("****EJERCICIO 6 - SOLUCION****", 2);
				PrintfCenterText("REGISTRO DE DATOS EN CAB1", 9);
				GoToXY({ 6, 12 });
				cout << "Digite un numero entero: ";
				int num = 0;
				cin >> num;
				cab01.Insertar(num);
				break;
			}
			case 2:
			{
				PaintInterface();
				PrintfCenterText("****EJERCICIO 6 - SOLUCION****", 2);
				PrintfCenterText("REGISTRO DE DATOS EN CAB2", 9);
				GoToXY({ 6, 12 });
				cout << "Digite un numero entero: ";
				int num = 0;
				cin >> num;
				cab02.Insertar(num);
				break;
			}
			case 3:
			{
				PaintInterface();
				PrintfCenterText("****EJERCICIO 6 - SOLUCION****", 2);
				MostrarLista(cab01, "DATOS EN CAB01");
				break;
			}
			case 4:
			{
				PaintInterface();
				PrintfCenterText("****EJERCICIO 6 - SOLUCION****", 2);
				MostrarLista(cab02, "DATOS EN CAB02");
				break;
			}
			case 5:
			{
				if (cab01.EstaVacia() || cab02.EstaVacia())
					PrintfErrOrSucc("Error: No hay datos en alguna de las listas");
				else
				{
					PaintInterface();
					PrintfCenterText("****EJERCICIO 6 - SOLUCION****", 2);
					PrintfCenterText("Cruzando listas....", 15);
					for (ListaSimple<int>::IteradorLista itr = cab02.GetInicio(); itr != cab02.GetFinal(); ++itr)
					{
						if (cab01.Contiene((*itr)))
							cab01.EliminarTodos((*itr));
					}
					Sleep(500);
					PrintfErrOrSucc("Listas cruzadas!!");
				}
				break;
			}
			case 6:
			{
				break;
			}
			default:
				PrintfErrOrSucc("Opcion no valida");
				break;
		}


	} while (op != 6);
}

void Ejercicio7()
{
	const char* opciones[] = {
		"1.Registrar datos en cab1",
		"2.Registrar datos en cab2",
		"3.Ver cab1",
		"4.Ver cab2",
		"5.Cruzar listas",
		"6.Salir"
	};

	int op = 0;

	do
	{
		PaintInterface();
		PrintfCenterText("****EJERCICIO 7 - SOLUCION****", 2);
		for (short i = 0; i < 6; i++)
		{
			GoToXY({ 15, 10 + i });
			cout << opciones[i];
		}

		GoToXY({ 15, 19 });
		cout << "Digite una opcion: ";
		cin >> op;

		switch (op)
		{
			case 1:
			{
				PaintInterface();
				PrintfCenterText("****EJERCICIO 7 - SOLUCION****", 2);
				PrintfCenterText("REGISTRO DE DATOS EN CAB1", 9);
				GoToXY({ 6, 12 });
				cout << "Digite un numero entero: ";
				int num = 0;
				cin >> num;
				cab1.Insertar(num);
				break;
			}
			case 2:
			{
				PaintInterface();
				PrintfCenterText("****EJERCICIO 7 - SOLUCION****", 2);
				PrintfCenterText("REGISTRO DE DATOS EN CAB1", 9);
				GoToXY({ 6, 12 });
				cout << "Digite un numero entero: ";
				int num = 0;
				cin >> num;
				GoToXY({ 6, 13 });
				cout << "Digite la accion (A/R): ";
				char accion = '\000';
				accion = _getche();
				while (toupper(accion) != 'R' && toupper(accion) != 'A')
				{
					PrintfErrOrSucc("Accion incorrecta, vuelva a digitarla....");
					GoToXY({ 6, 13 });
					cout << "Digite la accion (A/R): ";
					accion = _getche();
				}
				cab2.Insertar({ num, char(toupper(accion)) });
				break;
			}
			case 3:
			{
				if (cab1.EstaVacia())
					PrintfErrOrSucc("Error: No ha registrado elementos...");
				else
				{
					PaintInterface();
					PrintfCenterText("****EJERCICIO 7 - SOLUCION****", 2);
					MostrarLista(cab1, "DATOS EN CAB1");
				}
				break;
			}
			case 4:
			{
				if (cab2.EstaVacia())
					PrintfErrOrSucc("Error: No ha registrado elementos...");
				else
				{
					PaintInterface();
					PrintfCenterText("****EJERCICIO 7 - SOLUCION", 2);
					MostrarLista(cab2, "DATOS EN CAB2");
				}
				break;
			}
			case 5:
			{
				if (cab1.EstaVacia() || cab2.EstaVacia())
					PrintfErrOrSucc("Error: No ha registrado elementos...");
				else
				{
					PaintInterface();
					PrintfCenterText("****EJERCICIO 7 - SOLUCION", 2);
					PrintfCenterText("Actualizando cab1...", 15);
					for (ListaSimple<DatoAccion>::IteradorLista itr = cab2.GetInicio(); itr != cab2.GetFinal(); ++itr)
					{
						if ((*itr).accion == 'A')
							cab1.Insertar((*itr).dato);
						else if ((*itr).accion == 'R')
							cab1.Eliminar((*itr).dato);
					}

					Sleep(300);
					PrintfErrOrSucc("Cab1 actualizada...");
				}
				break;
			}
			case 6:
			{
				break;
			}
			default:
				PrintfErrOrSucc("Opcion no valida");
				break;
		}


	} while (op != 6);
}

void Ejercicio8()
{
	const char* opciones[] = {
		"1.Registrar un estudiante",
		"2.Ver estuduantes registrados",
		"3.Cerrar un grupo de estudiantes",
		"4.Ver estudiantes eliminados",
		"5.Regresar"
	};

	int op = 0;

	do
	{
		PaintInterface();
		PrintfCenterText("****EJERCICIO 8 - MANEJO DE ESTUDIANTES****", 2);
		for (short i = 0; i < 5; i++)
		{
			GoToXY({ 15, 10 + i });
			cout << opciones[i];
		}

		GoToXY({ 15, 19 });
		cout << "Digite una opcion: ";
		cin >> op;

		switch (op)
		{
			case 1:
			{
				RegistrarEstudiante();
				break;
			}
			case 2:
			{
				if (listaEstudiantes.EstaVacia())
					PrintfErrOrSucc("Error: No ha registrado estudiantes...");
				else
				{
					PaintInterface();
					PrintfCenterText("****EJERCICIO 8 - MANEJO DE ESTUDIANTES", 2);
					MostrarLista(listaEstudiantes, "ESTUDIANTES REGISTRADOS");
				}
				break;
			}
			case 3:
			{
				if (listaEstudiantes.EstaVacia())
					PrintfErrOrSucc("Error: No ha registrado estudiantes...");
				else
				{
					PaintInterface();
					PrintfCenterText("****EJERCICIO 8 - MANEJO DE ESTUDIANTES", 2);
					PrintfCenterText("CIERRE DE UN GRUPO DE ESTUDIANTES", 7);
					GoToXY({ 6, 12 });
					cout << "Digite el numero del grupo a cerrar: ";
					int num = 0;
					cin >> num;
					for (ListaSimple<Estudiante>::IteradorLista itr = listaEstudiantes.GetInicio(); itr != listaEstudiantes.GetFinal(); ++itr)
					{
						if ((*itr).grupo == num)
						{
							estudiantesBorrados.Insertar((*itr).nombre);
							listaEstudiantes.Eliminar((*itr));
						}
					}
				}
				break;
			}
			case 4:
			{
				if (estudiantesBorrados.EstaVacia())
					PrintfErrOrSucc("Error: no hay registro de estudiantes eliminados...");
				else
				{
					PaintInterface();
					PrintfCenterText("****EJERCICIO 8 - MANEJO DE ESTUDIANTES", 2);
					MostrarLista(estudiantesBorrados, "ESTUDIANTES ELIMINADOS");
				}
				break;
			}
			case 5:
			{
				break;
			}
			default:
				PrintfErrOrSucc("Opcion no valida...");
				break;
		}

	} while (op != 5);
}

void RegistrarEstudiante()
{
	PaintInterface();
	PrintfCenterText("****EJERCICIO 8 - MANEJO DE ESTUDIANTES****", 2);
	PrintfCenterText("REGISTRO DE ESTUDUANTE", 9);

	Estudiante estudiante;
	GoToXY({ 6, 12 });
	cout << "Digite el codigo del estudiante: ";
	cin >> estudiante.codigo;
	GoToXY({ 6, 13 });
	cout << "Digite el nombre del estudiante: ";
	cin.ignore();
	cin.getline(estudiante.nombre, 50, '\n');
	GoToXY({ 6, 14 });
	cout << "Digite el grupo del estudiante: ";
	cin >> estudiante.grupo;
	GoToXY({ 6, 15 });
	cout << "Digite el semetre del estudiante: ";
	cin >> estudiante.semestre;

	listaEstudiantes.Insertar(estudiante);
}

template<class T> void MostrarLista(const ListaSimple<T>& lista, string titulo)
{
	PrintfCenterText(titulo, 9);
	Iterador<T> itr = lista.GetInicio();
	short count = 0;
	GoToXY({ 6, 12 });
	while (itr != lista.GetFinal())
	{
		cout << (*itr);
		++itr;
		cout << ((itr != lista.GetFinal()) ? ", " : ".");
		++count;
	}
	PrintfCenterText("Datos registrados: ", 27);
	cout << count;
	PrintfErrOrSucc("Presione una tecla para continuar....");
	_getch();
}

void MostrarLista(const ListaSimple<Medicamento>& lista, string titulo)
{
	PrintfCenterText(titulo, 7);
	Nodo<Medicamento>* q = lista.GetCab();
	short count = 0;

	GoToXY({ 4, 10 });
	cout << "Referencia";
	GoToXY({ 18, 10 });
	cout << "Nombre";
	GoToXY({ 31, 10 });
	cout << "Presentacion";
	GoToXY({ 48, 10 });
	cout << "CantMax";
	GoToXY({ 58, 10 });
	cout << "CantMin";
	GoToXY({ 68, 10 });
	cout << "CantExt";

	while (q != nullptr)
	{
		GoToXY({ 4, 12 + count });
		cout << q->GetDato().referencia;
		GoToXY({ 18, 12 + count });
		cout << q->GetDato().nombre;
		GoToXY({ 31, 12 + count });
		cout << q->GetDato().presentacion;
		GoToXY({ 48, 12 + count });
		cout << q->GetDato().cantidadMaxima;
		GoToXY({ 58, 12 + count });
		cout << q->GetDato().cantidadMinima;
		GoToXY({ 68, 12 + count });
		cout << q->GetDato().cantidadExistente;
		q = q->GetSgte();
		count++;
	}
	PrintfCenterText("Datos registrados: ", 27);
	cout << count;
	PrintfErrOrSucc("Presione una tecla para continuar....");
	_getch();
}

void MostrarLista(const ListaSimple<ProductoEnBodega>& lista, string titulo)
{
	PrintfCenterText(titulo, 7);
	Nodo<ProductoEnBodega>* q = lista.GetCab();
	short count = 0;

	GoToXY({ 4, 10 });
	cout << "Referencia";
	GoToXY({ 30, 10 });
	cout << "Nombre";
	GoToXY({ 50, 10 });
	cout << "Cantidad en bodega";

	while (q != nullptr)
	{
		GoToXY({ 4, 12 + count });
		cout << q->GetDato().codigo;
		GoToXY({ 30, 12 + count });
		cout << q->GetDato().nombre;
		GoToXY({ 50, 12 + count });
		cout << q->GetDato().cantidad;
		q = q->GetSgte();
		count++;
	}
	PrintfCenterText("Datos registrados: ", 27);
	cout << count;
	PrintfErrOrSucc("Presione una tecla para continuar....");
	_getch();
}

void MostrarLista(const ListaSimple<ProductoVendido>& lista, string titulo)
{
	PrintfCenterText(titulo, 7);

	GoToXY({ 4, 10 });
	cout << "Referencia";
	GoToXY({ 30, 10 });
	cout << "Ventas";
	short count = 0;
	for (ListaSimple<ProductoVendido>::IteradorLista itr = lista.GetInicio(); itr != lista.GetFinal(); ++itr)
	{
		GoToXY({ 4, 12 + count });
		cout << (*itr).codigo;
		GoToXY({ 30, 12 + count });
		cout << (*itr).ventas;
		count++;
	}

	PrintfCenterText("Datos registrados: ", 27);
	cout << count;
	PrintfErrOrSucc("Presione una tecla para continuar....");
	_getch();
}

void MostrarLista(const ListaSimple<DatoAccion>& lista, string titulo)
{
	PrintfCenterText(titulo, 7);
	Nodo<DatoAccion>* q = cab2.GetCab();
	short count = 0;

	GoToXY({ 10, 10 });
	cout << "Numero";
	GoToXY({ 30, 10 });
	cout << "Accion";

	while (q != nullptr)
	{
		GoToXY({ 10, 12 + count });
		cout << q->GetDato().dato;
		GoToXY({ 30, 12 + count });
		cout << q->GetDato().accion;
		q = q->GetSgte();
		++count;
	}

	PrintfCenterText("Datos registrados: ", 27);
	cout << count;
	PrintfErrOrSucc("Presione una tecla para continuar....");
	_getch();
}

void MostrarLista(const ListaSimple<Estudiante>& lista, string titulo)
{
	PrintfCenterText(titulo, 7);
	short count = 0;

	GoToXY({ 4, 9 });
	cout << "Codigo";
	GoToXY({ 20, 9 });
	cout << "Nombre";
	GoToXY({ 45, 9 });
	cout << "Grupo";
	GoToXY({ 55, 9 });
	cout << "Semestre";

	ListaSimple<Estudiante>::IteradorLista q = lista.GetInicio();
	for (; q != lista.GetFinal(); ++q)
	{
		GoToXY({ 4, 12 + count });
		cout << (*q).codigo;
		GoToXY({ 20, 12 + count });
		cout << (*q).nombre;
		GoToXY({ 45, 12 + count });
		cout << (*q).grupo;
		GoToXY({ 55, 12 + count });
		cout << (*q).semestre;
		++count;
	}

	PrintfCenterText("Datos registrados: ", 27);
	cout << count;
	PrintfErrOrSucc("Presione una tecla para continuar....");
	_getch();
}
