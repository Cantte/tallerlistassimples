#ifndef NODO_H
#define NODO_H

template<class T> class Nodo
{
public:
	Nodo(T dato, Nodo<T>* sgte)
	{
		m_dato = dato;
		m_sgte = sgte;
	}

	Nodo(T dato)
	{
		m_dato = dato;
		m_sgte = nullptr;
	}

	T GetDato() const
	{
		return m_dato;
	}

	Nodo<T>* GetSgte() const
	{
		return m_sgte;
	}

	void SetDato(T dato)
	{
		m_dato = dato;
	}

	void SetSgte(Nodo<T>* sgte)
	{
		m_sgte = sgte;
	}

	bool operator==(const Nodo<T>* nodo)
	{
		this == nodo;
	}

private:
	T			m_dato;
	Nodo<T>*	m_sgte;
};

#endif // !NODO_H
