#ifndef ITERADOR_H
#define ITERADOR_H

#include "Nodo.h"

template <class T> class Iterador
{
public:
	Iterador(Nodo<T>* nodo)
	{
		m_nodo = nodo;
	}

	Iterador<T> operator++()
	{
		m_nodo = m_nodo->GetSgte();
		return *this;
	}

	bool operator==(const Iterador<T>& anIterator)
	{
		return m_nodo == anIterator.m_nodo;
	}

	bool operator!=(const Iterador<T>& anIterator)
	{
		return !(*this == anIterator);
	}

	T operator*()
	{
		return m_nodo->GetDato();
	}

private:
	Nodo<T>* m_nodo;
};

#endif // !ITERADOR_H

